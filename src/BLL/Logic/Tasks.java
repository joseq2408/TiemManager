/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL.Logic;

/**
 *
 * @author Soporte_TI
 * Esta clase se encarga de manejar las tareas pendiendtes
 */
public class Tasks {
    
    private String Nombre;
    private int Periodicidad; 
    private Boolean Completado;

    /**
     * Constructor de la clase Tasks
     * Aqui definimos las tareas a tomar en la lista de tareas, se crea con Completado en False
     * @param Nombre Nombre de la Tarea
     * @param Periodicidad 1-Diario / 2-Semanal / 3-Mensual / 4-Trimestral / 5-Semestral / 6-Anual /7- Personalizada
     */
    public Tasks(String Nombre, int Periodicidad) {
        this.Nombre = Nombre;
        this.Periodicidad = Periodicidad;
        this.Completado = false;
    }


    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public int getPeriodicidad() {
        return Periodicidad;
    }

    public void setPeriodicidad(int Periodicidad) {
        this.Periodicidad = Periodicidad;
    }

    public Boolean getCompletado() {
        return Completado;
    }

    public void setCompletado(Boolean Completado) {
        this.Completado = Completado;
    }
    
    
    
}
