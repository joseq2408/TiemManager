/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL.Logic;

/**
 *
 * @author Soporte_TI
 */
public class Event {
    
    private String Usuario;
    private String HoraInicio;
    private String HoraFinal;
    private String Detalle;

    /**
     * Aqui definimos los registros que se van a guardar en nuestro archivo de Resgister
     * @param Usuario Nombre del usuario que realiza el evento
     * @param HoraInicio Hora en la que incia el evento
     * @param Detalle Nombre del evento, que corresponde al nombre de una tarea
     */
    
    public Event(String Usuario, String HoraInicio, String Detalle) {
        this.Usuario = Usuario;
        this.HoraInicio = HoraInicio;
        this.Detalle = Detalle;
    }

    @Override
    /**
     * Genera un String listo para ser registrado
     * @return Registro Listo
     */
    public String toString() {
        String Cadena;
        Cadena = this.Usuario + ";" + this.HoraInicio + ";" + this.HoraFinal + ";" + this.Detalle;
        return Cadena;
    }

    /**
     * Constructor vacio
     */
    public Event() {
    }

    public String getUsuario() {
        return Usuario;
    }

    public String getHoraInicio() {
        return HoraInicio;
    }

    public String getHoraFinal() {
        return HoraFinal;
    }

    public String getDetalle() {
        return Detalle;
    }

    public void setHoraFinal(String HoraFinal) {
        this.HoraFinal = HoraFinal;
    }
    
    
}
