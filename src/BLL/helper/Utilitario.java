package BLL.helper;

import GUI.Admin.*;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Scanner;
import javax.swing.JDialog;

public class Utilitario {

    public static int capturarValorEntero(String textoPeticion) {

        int valorRetorno = 0;

        boolean continuar = true;

        Scanner teclado = new Scanner(System.in);

        while (continuar) {

            try {
                System.out.println(textoPeticion);
                teclado = new Scanner(System.in);
                valorRetorno = teclado.nextInt();
                continuar = false;
            } catch (Exception e) {
                System.out.println("\nValor ingresado incorrecto, debe ingresar un numero entero!!");
            }

        }

        return valorRetorno;

    }

    public static double capturarValorDouble(String textoPeticion) {

        double valorRetorno = 0;

        boolean continuar = true;

        Scanner teclado = new Scanner(System.in);

        while (continuar) {

            try {
                System.out.println(textoPeticion);
                teclado = new Scanner(System.in);
                valorRetorno = teclado.nextDouble();
                continuar = false;
            } catch (Exception e) {
                System.out.println("\nValor ingresado incorrecto, debe ingresar un numero!!");
            }

        }

        return valorRetorno;

    }

    public static String capturarValorString(String textoPeticion) {

        String valorRetorno = "";

        Scanner teclado = new Scanner(System.in);

        System.out.println(textoPeticion);
        valorRetorno = teclado.nextLine();

        return valorRetorno;

    }

    public static void MostarFormEvents(String pUsuario) {
        FrmEvents ventanasecundaria = new FrmEvents(pUsuario);
        ventanasecundaria.pack();
        ventanasecundaria.setVisible(true);
        ventanasecundaria.setLocationRelativeTo(null);
    }

    public static void MostarFormReports() {
        FrmReports ventanasecundaria = new FrmReports();
        ventanasecundaria.pack();
        ventanasecundaria.setVisible(true);
        ventanasecundaria.setLocationRelativeTo(null);
    }

    public static void MostarFormConfig() {
        FrmConfig ventanasecundaria = new FrmConfig();
        ventanasecundaria.pack();
        ventanasecundaria.setVisible(true);
        ventanasecundaria.setLocationRelativeTo(null);
    }

    public static void MostarFormRealTime() {
        FrmRealTime ventanasecundaria = new FrmRealTime();
        ventanasecundaria.pack();
        ventanasecundaria.setVisible(true);
        ventanasecundaria.setLocationRelativeTo(null);
    }

    public static void MostarFormTasks() {
        FrmTasks ventanasecundaria = new FrmTasks();
        ventanasecundaria.pack();
        ventanasecundaria.setVisible(true);
        ventanasecundaria.setLocationRelativeTo(null);
    }

    public static void MostarJDialogNewTask() {
        JDlogNewTask ventanasecundaria = new JDlogNewTask(new javax.swing.JFrame(), true);
        ventanasecundaria.pack();
        ventanasecundaria.setVisible(true);
        ventanasecundaria.setLocationRelativeTo(null);
    }

    /**
     *
     * @return Retorna la Hora del Sistema
     */
    public static String TimeStamp() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        String hora = formatter.format(date);
        return hora;
    }
}
